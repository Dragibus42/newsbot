#! /bin/bash

NEWS_JAR="./News.jar"
NEWS_OUT_ERR="./news.out.err"
NEWS_PID="./news.pid"

start()
{
    if [ -f $NEWS_PID ];
    then
        echo "News already running"
    else
	if [ -f $NEWS_JAR ];
	then
            nohup java -jar $NEWS_JAR >$NEWS_OUT_ERR 2>&1 &
            echo $! > $NEWS_PID
	    echo "Started"
	else
	    echo "Error: Unable to access jarfile $NEWS_JAR"
	fi
    fi
}

stop()
{
    if [ -f $NEWS_PID ];
    then
        kill -9 `cat $NEWS_PID`
	rm $NEWS_PID
	echo "Stopped"
    else
        echo "Not started"
    fi
}

help()
{
    echo "usage: news.sh [help|restart|start|stop]"
}

case "$1" in
    "start")
	start
	;;
    "stop")
	stop
	;;
    "restart")
	stop
	start
	;;
    "help"|*)
	help
	;;
esac
