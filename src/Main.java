import Util.GetInformation;
import Util.Log;

/**
 * Created by damien on 17/04/16.
 */
public class Main
{
    public static void main(String[] args)
    {
        Runtime.getRuntime().addShutdownHook(new Thread()
        {
            @Override
            public void run()
            {
                Log.getInstance().close();
            }
        });
        new GetInformation().start();
    }
}