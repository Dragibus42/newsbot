package Entity;

import java.io.Serializable;

/**
 * Created by damien on 17/04/16.
 */
public class Article implements Serializable
{
    private String title;
    private String url;

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }
}
