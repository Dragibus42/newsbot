package Entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by damien on 17/04/16.
 */
public class WebSite implements Serializable
{
    private String name;
    private String url;
    private String cssQuery;
    private HashMap<String, Article> articles = new HashMap<>();
    private ArrayList<Article> articlesToSend = new ArrayList<>();
    private ArrayList<Article> articlesToKeep = new ArrayList<>();

    public void addArticles(Article article)
    {
        if (articles.get(article.getTitle()) == null)
            articlesToSend.add(article);
        else
            articlesToKeep.add(article);
    }

    public void cleanArticles()
    {
        articles.clear();
        for (Article article : articlesToSend)
            articles.put(article.getTitle(), article);
        for (Article article : articlesToKeep)
            articles.put(article.getTitle(), article);
        articlesToKeep.clear();
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getCssQuery()
    {
        return cssQuery;
    }

    public void setCssQuery(String cssQuery)
    {
        this.cssQuery = cssQuery;
    }

    public ArrayList<Article> getArticlesToSend()
    {
        return articlesToSend;
    }
}
