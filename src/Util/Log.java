package Util;

import java.io.BufferedWriter;
import java.io.FileWriter;

/**
 * Created by damien on 01/08/16.
 */
public class Log
{
    private static Log instance;
    private BufferedWriter bufferedWriter;
    private FileWriter fileWriter;

    private Log()
    {
        try
        {
            fileWriter = new FileWriter(Configuration.getInstance().getValue("LOG_FILE"), true);
            bufferedWriter = new BufferedWriter(fileWriter);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static Log getInstance()
    {
        if (instance == null)
        {
            instance = new Log();
            return instance;
        }
        return instance;
    }

    public void write(String toWrite)
    {
        try
        {
            bufferedWriter.write(toWrite);
            bufferedWriter.flush();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void close()
    {
        try
        {
            fileWriter.close();
            bufferedWriter.close();
        }
        catch (Exception e)
        {
        }
    }
}
