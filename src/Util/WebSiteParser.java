package Util;

import Entity.Article;
import Entity.WebSite;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by damien on 17/04/16.
 */
public class WebSiteParser
{
    public void getNews(ArrayList<WebSite> webSites)
    {
        for (WebSite webSite : webSites)
        {
            Document document = loadDocument(webSite);
            if (document == null)
                continue;
            Elements elements = document.select(webSite.getCssQuery());
            for (Element element : elements)
            {
                Article article = new Article();
                article.setTitle(element.text());
                String url = element.attr("href").replace(" ", "");
                if (url.length() > 0 && url.charAt(0) == '/')
                    url = webSite.getUrl().substring(0, webSite.getUrl().indexOf('/', 7)) + url;
                article.setUrl(url);
                webSite.addArticles(article);
            }
            webSite.cleanArticles();
        }
    }

    private Document loadDocument(WebSite webSite)
    {
        Document document = null;
        try
        {
            document = Jsoup.connect(webSite.getUrl()).get();
        }
        catch (Exception e)
        {
            try
            {
                StringBuilder result = new StringBuilder();
                URL url = new URL(webSite.getUrl());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:44.0) Gecko/20100101 Firefox/44.0");
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String line;
                while ((line = bufferedReader.readLine()) != null)
                    result.append(line);
                bufferedReader.close();
                document = Jsoup.parse(result.toString());
            }
            catch (Exception ex)
            {
                System.err.println("An error occurred, check your connection or it may be due to the website \"" + webSite.getName() + "\"");
            }
        }
        return document;
    }
}
