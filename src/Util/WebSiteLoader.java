package Util;

import Entity.WebSite;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by damien on 17/04/16.
 */
public class WebSiteLoader
{
    private ArrayList<WebSite> webSites = new ArrayList<>();
    private final String webSiteListPath = Configuration.getInstance().getValue("WEBSITE_LIST");

    public WebSiteLoader()
    {
        loadWebSites(webSiteListPath);
    }

    private void loadWebSites(String filePath)
    {
        try {
            File inputFile = new File(filePath);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();

            parser(doc);
        } catch (Exception e) {
            System.err.println(filePath + " no found");
        }
    }

    private void parser(Document document)
    {
        NodeList nodeList = document.getElementsByTagName("websites").item(0).getChildNodes();
        for (int i = 0; i < nodeList.getLength(); ++i)
        {
            if (nodeList.item(i).getNodeType() == Node.ELEMENT_NODE)
            {
                Element element = (Element) nodeList.item(i);
                WebSite webSite = new WebSite();
                webSite.setName(element.getElementsByTagName("name").item(0).getTextContent());
                webSite.setUrl(element.getElementsByTagName("url").item(0).getTextContent());
                webSite.setCssQuery(element.getElementsByTagName("cssQuery").item(0).getTextContent());

                webSites.add(webSite);
            }
        }
    }

    public ArrayList<WebSite> getWebSites()
    {
        return webSites;
    }
}
