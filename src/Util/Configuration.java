package Util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;

/**
 * Created by damien on 17/04/16.
 */
public class Configuration
{
    private static Configuration instance;
    private final String configurationPath = "config.ini";
    private final String[] criticalValues = new String[]{"SEND_SCRIPT", "WEBSITE_LIST", "EXECUTOR", "LOG_FILE"};
    private HashMap<String, String> configurationTable;

    private Configuration()
    {
        loadConfiguration();
        isValid();
    }

    public static Configuration getInstance()
    {
        if (instance == null)
        {
            instance = new Configuration();
            return instance;
        }
        return instance;
    }

    private void loadConfiguration()
    {
        configurationTable = new HashMap<>();
        try
        {
            FileReader fileReader = new FileReader(new File(configurationPath));
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                if (line.isEmpty() || line.charAt(0) == '#')
                    continue;
                line = line.replaceAll("\t", "");
                line = line.replaceAll(" ", "");
                String[] words = line.split("=");
                if (words.length == 2)
                    configurationTable.put(words[0].toUpperCase(), words[1]);
            }
            bufferedReader.close();
            fileReader.close();
        }
        catch (Exception e)
        {
            System.err.println("Unable to load config.ini");
            System.exit(1);
        }
    }

    public void isValid()
    {
        for (String criticalValue : criticalValues)
        {
            if (configurationTable.get(criticalValue.toUpperCase()) == null)
            {
                System.err.println("The value '" + criticalValue + "' is missing on config.ini");
                System.exit(1);
            }
        }
    }

    public String getValue(String key)
    {
        return configurationTable.get(key);
    }
}
