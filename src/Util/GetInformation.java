package Util;

import Entity.WebSite;

import java.util.ArrayList;

/**
 * Created by damien on 17/04/16.
 */
public class GetInformation
{
    private WebSiteLoader webSiteLoader = new WebSiteLoader();
    private WebSiteParser webSiteParser = new WebSiteParser();
    private Long refreshDelay = 60L;
    private ArrayList<WebSite> webSites;
    private Sender sender = new Sender();

    public GetInformation()
    {
        setRefreshDelay();
        webSites = webSiteLoader.getWebSites();
    }

    private void setRefreshDelay()
    {
        try
        {
            refreshDelay = Long.parseLong(Configuration.getInstance().getValue("REFRESH"));
        }
        catch (Exception e) {}
    }

    public void start()
    {
        webSiteParser.getNews(webSites);
        for (WebSite webSite : webSites)
            webSite.getArticlesToSend().clear();
        try
        {
            Thread.sleep(refreshDelay * 1000);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        do
        {
            try
            {
                webSiteParser.getNews(webSites);
                sender.sendArticles(webSites);
                Thread.sleep(refreshDelay * 1000);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        } while (true);
    }
}
