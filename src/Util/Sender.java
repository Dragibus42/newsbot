package Util;

import Entity.Article;
import Entity.WebSite;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by damien on 17/04/16.
 */
public class Sender
{
    private final String executor = Configuration.getInstance().getValue("EXECUTOR");
    private final String scriptPath = Configuration.getInstance().getValue("SEND_SCRIPT");

    public void sendArticles(ArrayList<WebSite> webSites)
    {
        for (WebSite webSite : webSites)
        {
            ArrayList<Article> articles = webSite.getArticlesToSend();
            for (Article article : articles)
                sendArticle(webSite, article);
            webSite.getArticlesToSend().clear();
        }
    }

    private void sendArticle(WebSite webSite, Article article)
    {
        try
        {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date current = new Date();
            String date = dateFormat.format(current);

            Log.getInstance().write(date + System.lineSeparator() + webSite.getName() + System.lineSeparator() + article.getTitle() + System.lineSeparator() + article.getUrl() + System.lineSeparator() + System.lineSeparator());
            Runtime.getRuntime().exec(new String[]{executor, scriptPath, webSite.getName(), article.getTitle(), article.getUrl()});
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
